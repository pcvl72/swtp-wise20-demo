package de.thm.swtp.demo;

public interface MessageService {

    MessageEntity saveNewMessage(String text);
    MessageEntity getMessageById(Integer id);

}
