package de.thm.swtp.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MessageController {

    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping(path = "/test/{id}")
    public ResponseEntity<MessageEntity> getMethode(@PathVariable int id) {
        MessageEntity messageEntity = this.messageService.getMessageById(id);
        if (messageEntity == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(messageEntity);
    }

    @PostMapping(path = "/test")
    public MessageEntity postMethode(@RequestBody String messageText) {
        return this.messageService.saveNewMessage(messageText);
    }
}
