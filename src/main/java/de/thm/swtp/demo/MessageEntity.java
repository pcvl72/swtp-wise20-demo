package de.thm.swtp.demo;

import org.springframework.data.annotation.Id;

public class MessageEntity {

    @Id
    private Integer id;

    private String text;

    public MessageEntity(Integer id, String text) {
        this.id = id;
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public String getText() {
        return text;
    }

}
