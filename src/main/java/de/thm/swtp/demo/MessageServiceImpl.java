package de.thm.swtp.demo;

import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {

    private final MessageEntityRepository repository;

    public MessageServiceImpl(MessageEntityRepository repository) {
        this.repository = repository;
    }

    @Override
    public MessageEntity saveNewMessage(String text) {
        return this.repository.save(new MessageEntity(null, text));
    }

    @Override
    public MessageEntity getMessageById(Integer id) {
        return this.repository.findById(id).orElse(null);
    }
}
