package de.thm.swtp.demo;

import org.springframework.data.repository.CrudRepository;

public interface MessageEntityRepository extends CrudRepository<MessageEntity, Integer> {}
