CREATE TABLE message_entity
(
    id   SERIAL PRIMARY KEY,
    text CHARACTER VARYING(255)
)