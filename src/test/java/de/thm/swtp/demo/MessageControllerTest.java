package de.thm.swtp.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = { MessageController.class })
public class MessageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MessageService service;

    @Test
    public void returnsTestMessage() throws Exception {

        doAnswer(invocationOnMock -> {
            Integer id = invocationOnMock.getArgument(0, Integer.class);
            return new MessageEntity(id, "Test" + id + "!");
        }).when(service).getMessageById(anyInt());

        mockMvc
                .perform(MockMvcRequestBuilders.get("/test/43"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(43))
                .andExpect(jsonPath("$.text").value("Test43!"));

    }

    @Test
    public void returnsHttp404IfMessageNotFound() throws Exception {

        // Wird kein "doAnswer" oder "when" etc... verwendet, liefert die Mock-methode immer "null"

        mockMvc
                .perform(MockMvcRequestBuilders.get("/test/1234567890"))
                .andExpect(status().isNotFound());

    }

    @Test
    public void shouldAcceptMessageString() throws Exception {

        doAnswer(invocationOnMock -> {
            String text = invocationOnMock.getArgument(0, String.class);
            return new MessageEntity(1, text);
        }).when(service).saveNewMessage(anyString());

        mockMvc
                .perform(MockMvcRequestBuilders.post("/test").content("Test1!"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.text").value("Test1!"));

        verify(service).saveNewMessage(anyString());

    }

}
