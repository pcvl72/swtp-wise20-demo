package de.thm.swtp.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MessageServiceImplTest {

    @Mock
    private MessageEntityRepository repository;

    private MessageServiceImpl impl;

    @BeforeEach
    public void setup() {
        impl = new MessageServiceImpl(repository);
    }

    @Test
    public void shouldGetMessage() {
        when(repository.findById(anyInt()))
                .thenReturn(Optional.of(new MessageEntity(1, "Blubtest1234")));

        assertThat(impl.getMessageById(1)).isNotNull();
    }

    @Test
    public void shouldGetMessageWithDoAnswer() {
        doAnswer(invocationOnMock -> {
            Integer id = invocationOnMock.getArgument(0, Integer.class);
            assertThat(id).isEqualTo(1);
            return Optional.of(new MessageEntity(id, "Blubtest1234"));
        }).when(repository).findById(anyInt());

        assertThat(impl.getMessageById(1)).isNotNull();
    }

    @Test
    public void shouldSaveMessage() {
        // Prepare
        when(repository.save(any(MessageEntity.class)))
                .thenReturn(new MessageEntity(2, "Test"));

        // Act
        MessageEntity actual = impl.saveNewMessage("Test");

        // Assert
        verify(repository, times(1)).save(any(MessageEntity.class));
        verify(repository, never()).delete(any(MessageEntity.class));

        assertThat(actual).isNotNull();
        assertThat(actual.getId()).isEqualTo(2);
        assertThat(actual.getText()).isEqualTo("Test");

    }

}
